<?php

namespace Database\Seeders;
use App\Models\User;
use App\Models\Karyawan;
use Hash;



use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Karyawan
        $user = User::create([
            'name' => 'Fahrul Ihsan',
            'email' => 'fahrul@example.com',
            'password' => Hash::make("fahrul123"),
            'role' => 'karyawan',
        ]);
        
        $user = User::create([
            'name' => 'Ferdian Samsudin',
            'email' => 'ferdian@example.com',
            'password' => Hash::make("ferdian123"),
            'role' => 'karyawan',
        ]);
        $user = Karyawan::create([
            'user_id' => '1',
            'alamat' => 'jl babenget no 6',
            'no_telpon' => 62898945934,
            'jumlah_cuti' => 7,
        ]);
        
        $user = Karyawan::create([
            'user_id' => '2',
            'alamat' => 'jl padjajaran no 6',
            'no_telpon' => 62898945987,
            'jumlah_cuti' => 20,
        ]);

        $user = User::create([
            'name' => 'Galih Purna',
            'email' => 'galih@example.com',
            'password' => Hash::make("galih123"),
            'role' => 'Super Admin',
        ]);
        
        $user = User::create([
            'name' => 'Rudi Darmawangsa',
            'email' => 'Rudi@example.com',
            'password' => Hash::make("galih123"),
            'role' => 'Staf HR',
        ]);
    }
}
